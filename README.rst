Python Perlin
=============

If nothing else, the alliteration is good.

perlin-0.1.0 includes perlin.py, the module with the PerlinNoise class;
demo.py, demoing Perlin noise in various dimensions with `pygame`_; and
prof.py, which I use to test out the speed of different code options.

**Note:** This is more of a "code leak" than whatcha call a "release".
Stuff works. Well, the important bits, anyway. No polishing included.

**Perlin noise** is a procedural texture desgined by `Ken Perlin`_. It's
(pseudo)random, it doesn't repeat. It's band-limited, so the texture has
a sort of uniform "roughness", and there are no large-scale features
when you look at it from afar. So it's pretty much the happenin' thing
to use when you have stuff you don't want to look flat.

This implementation of Perlin noise is based off the suggestions
Ken Perlin in his 1999 talk, `Making Noise`_. This module supports
creating noise in *any* number of dimensions.

.. image:: doc/pyperlin.png
  :width: 640
  :height: 480
  :alt: Obligatory screenshot. 2D Perlin texture and cross-section.

(That looks better when you're running the demo and stuff is moving.)


.. _pygame: https://web.archive.org/web/20100416184801/http://www.pygame.org/
.. _Ken Perlin: https://web.archive.org/web/20100416184801/http://mrl.nyu.edu/~perlin
.. _Making Noise: https://web.archive.org/web/20100416184801/http://www.noisemachine.com/talk1

.. meta::
  :description: PerlinNoise objects for Python.
  :keywords: perlin, noise, procedural, texture, python, dimensions
